package com.android.potioneer.ui.main


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.potioneer.custom.FragmentNavigation
import com.example.touch100.touchwifieservice.R
import com.example.touch100.touchwifieservice.main.*
import com.example.touch100.touchwifieservice.main.menuWIFI.WifiFragment

class HolderFragment : Fragment() , FragmentNavigation {


    companion object {
        fun newInstance(tabs: Int): HolderFragment {
            val fragment = HolderFragment()
            val args = Bundle()
            args.putInt("tab",tabs)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_holder, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tabs = arguments?.getInt("tab",0)
        if(childFragmentManager.findFragmentById(R.id.holderFrame)==null){
            var fragment: Fragment? = null
            when (tabs) {
                R.id.ivFirst -> fragment = WifiFragment.newInstance()
                R.id.ivSecond  -> fragment = AnalyzerFragment.newInstance()
                R.id.ivThird -> fragment = PingFragment.newInstance()
                R.id.ivFourth  -> fragment = FtpFragment.newInstance()
                R.id.ivFift  -> fragment = SpeedFragment.newInstance()
                else -> {
                }
            }

            childFragmentManager.beginTransaction()
                    .replace(R.id.holderFrame, fragment!!)
                    .commitAllowingStateLoss()

        }
    }

    override fun open(fragment: Fragment) {
        childFragmentManager.beginTransaction()
                .addToBackStack(null)
                .setReorderingAllowed(true)
                .add(R.id.holderFrame, fragment)
                .commit()

    }

    override fun replace(fragment: Fragment, addToBackStack: Boolean) {
        if (addToBackStack) {
            childFragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .setReorderingAllowed(true)
                    .replace(R.id.contentContainer, fragment)
                    .commitAllowingStateLoss()
        } else {
            childFragmentManager.beginTransaction()
                    .setReorderingAllowed(true)
                    .replace(R.id.contentContainer, fragment)
                    .commitAllowingStateLoss()
        }
    }

    override fun navigateBack() {
        if(fragmentManager!!.backStackEntryCount<2){
            fragmentManager!!.popBackStack()
        }else{
            activity!!.onBackPressed()
        }
    }






}