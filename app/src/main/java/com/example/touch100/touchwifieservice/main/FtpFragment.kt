package com.example.touch100.touchwifieservice.main

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.touch100.touchwifieservice.R

class FtpFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ftp, container, false)
    }


    companion object {
        @JvmStatic
        fun newInstance() =
                FtpFragment().apply {
                    arguments = Bundle().apply {
                    }
                }
    }
}
