package com.example.touch100.touchwifieservice.util

import android.annotation.SuppressLint
import android.content.Context

@SuppressLint("StaticFieldLeak")
object Contextor{
    lateinit var mContext: Context

    fun init(context: Context){
        this.mContext = context
    }
}