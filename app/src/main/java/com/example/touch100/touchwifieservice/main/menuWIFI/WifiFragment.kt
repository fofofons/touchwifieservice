package com.example.touch100.touchwifieservice.main.menuWIFI

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.example.touch100.touchwifieservice.R
import com.example.touch100.touchwifieservice.base.BaseFragment
import com.example.touch100.touchwifieservice.main.menuWIFI.adapter.AdapterWIFIList
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import java.math.BigInteger
import java.net.InetAddress
import java.net.UnknownHostException
import java.nio.ByteOrder

class WifiFragment : BaseFragment() {

    lateinit var mActivity: Activity
    private val time: Long = 2000
    var mainWifi: WifiManager? = null
    var size = 0
    private var info: WifiInfo? = null
    private var wifiReciever : WifiScanReceiver? = null

    var wifiName : TextView? = null
    var tvDBM : TextView? = null
    var tvLinkSpeed : TextView? = null
    var tvMAC : TextView? = null
    var tvIP : TextView? = null
    var tvChannel : TextView? = null
    var listWifi : RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_wifi, container, false)
        mActivity = activity!!
        initInstance(view)
        return view
    }

    private fun initInstance(view: View) {
        mainWifi = mActivity.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiReciever = WifiScanReceiver()
        val layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        checkPermission()
        wifiName = view.findViewById(R.id.wifiName) as TextView
        tvDBM = view.findViewById(R.id.tvDBM) as TextView
        tvLinkSpeed = view.findViewById(R.id.tvLinkSpeed) as TextView
        tvMAC = view.findViewById(R.id.tvMAC) as TextView
        tvIP = view.findViewById(R.id.tvIP) as TextView
        tvChannel = view.findViewById(R.id.tvChannel) as TextView
        wifiName!!.text = String.format("Connected: %s",mainWifi!!.connectionInfo.ssid)
        tvDBM!!.text = String.format("%s -dbm",mainWifi!!.connectionInfo.describeContents())
        tvLinkSpeed!!.text = String.format("LinkSpeed %s Mbps.",mainWifi!!.connectionInfo.linkSpeed)
        tvMAC!!.text = String.format("%s",mainWifi!!.connectionInfo.macAddress)
        tvIP!!.text = String.format("%s",wifiIpAddress(mActivity))
        listWifi = view.findViewById(R.id.listWifi)
        listWifi!!.layoutManager = layoutManager
    }

    private inner class WifiScanReceiver : BroadcastReceiver() {
        override fun onReceive(c: Context, intent: Intent) {
            if (mainWifi!!.isWifiEnabled) {
                info = mainWifi!!.connectionInfo
                wifiName!!.text = String.format("Connected: %s",mainWifi!!.connectionInfo.ssid)
                tvDBM!!.text = String.format("%s -dbm",mainWifi!!.connectionInfo.describeContents())
                tvLinkSpeed!!.text = String.format("LinkSpeed %s Mbps.",mainWifi!!.connectionInfo.linkSpeed)
                tvMAC!!.text = String.format("%s",mainWifi!!.connectionInfo.macAddress)
                tvIP!!.text = String.format("%s",wifiIpAddress(mActivity))
                val wifiresults = mainWifi!!.scanResults
                val adapterWIFIList = AdapterWIFIList(mActivity, wifiresults)
                adapterWIFIList.setOnClickWIFI(object : AdapterWIFIList.OnWifiClickListener{
                    override fun onClickWifi(index: Int) {

                    }

                })
                listWifi!!.removeAllViewsInLayout()
                listWifi!!.adapter = adapterWIFIList
                for (scanResult in wifiresults) {
                    Log.i("results", scanResult.SSID)
                    if(String.format("\"%s\"",scanResult.SSID) == mainWifi!!.connectionInfo.ssid) {
                        val level = WifiManager.calculateSignalLevel(scanResult.level, 5)
                        tvDBM!!.text = String.format("%s -dbm",level)
                        tvChannel!!.text = String.format("Channel %s",scanResult.channelWidth)
                    }
                }
            }
        }
    }

    fun Connect(networkSSID: String ) {
        mainWifi!!.disconnect()
        val wifiConfiguration = WifiConfiguration()
        wifiConfiguration.SSID = String.format("\"%s\"", networkSSID)
        wifiConfiguration.hiddenSSID = true
        wifiConfiguration.status = WifiConfiguration.Status.ENABLED
        wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
        val netId = mainWifi!!.addNetwork(wifiConfiguration)
        Log.i("WifiPreference", networkSSID)
        Log.i("WifiPreference", "add Network returned $netId")
        val checkEnableWifi = mainWifi!!.enableNetwork(netId, true)
        mainWifi!!.reconnect()
        Log.i("WifiPreference", "enableNetwork returned $checkEnableWifi")
        if(checkEnableWifi) {

        }else{
            Toast.makeText(mActivity.getApplicationContext(), "Connection failed, please try again.", Toast.LENGTH_LONG).show()
        }
    }

    fun checkPermission(){
        Dexter.withActivity(mActivity)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<com.karumi.dexter.listener.PermissionRequest>?, token: PermissionToken?) {
                        checkPermission()
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        Log.i("","")
                        val filter = IntentFilter()
                        filter.addAction(WifiManager.RSSI_CHANGED_ACTION)
                        filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
                        mActivity.registerReceiver(wifiReciever , filter)
                        mainWifi!!.startScan()
                    }
                }).check()
    }

    fun wifiIpAddress(context: Context): String {
        val wifiManager = mActivity.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        var ipAddress = wifiManager.getConnectionInfo().getIpAddress()
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress)
        }

        val ipByteArray = BigInteger.valueOf(ipAddress.toLong()).toByteArray()


        var ipAddressString: String
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress()
        } catch (ex: UnknownHostException) {
            Log.e("WIFIIP", "Unable to get host address.")
            ipAddressString = ""
        }

        return ipAddressString
    }

    companion object {
        @JvmStatic
        fun newInstance() = WifiFragment().apply {
                    arguments = Bundle().apply {
//                        putString(ARG_PARAM1, param1)
//                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
