package com.example.touch100.touchwifieservice.base

import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View

open class BaseFragment(): Fragment(), View.OnClickListener{

    fun AppCompatActivity.replaceFragment(layoutId: Int, fragment: Fragment){
        supportFragmentManager.beginTransaction().replace(layoutId, fragment).commit()
    }
    fun AppCompatActivity.replaceFragmentwithBackStack(layoutId: Int, fragment: Fragment, tag: String?) {
        supportFragmentManager.beginTransaction().replace(layoutId, fragment, tag)
    }
    override fun onClick(p0: View?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}