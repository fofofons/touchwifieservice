package com.example.touch100.touchwifieservice

import android.app.Application
import com.example.touch100.touchwifieservice.util.Contextor

class MainApplication : Application(){

    override fun onCreate() {
        super.onCreate()
        Contextor.init(this)
    }

    override fun onTerminate() {
        super.onTerminate()
    }

}