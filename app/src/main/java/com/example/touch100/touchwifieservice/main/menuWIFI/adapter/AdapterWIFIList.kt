package com.example.touch100.touchwifieservice.main.menuWIFI.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.net.wifi.ScanResult
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.touch100.touchwifieservice.R


class AdapterWIFIList (context: Context, wifiList: List<ScanResult>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var listener: OnWifiClickListener? = null
    private var wifiList: List<ScanResult> = wifiList as List<ScanResult>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_wifi_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val scanResult = wifiList.get(position)
        val ViewHolder = holder as ViewHolder
        setViewList(ViewHolder,scanResult , position)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {
        return wifiList.size
    }

    @SuppressLint("NewApi")
    private fun setViewList(holder: ViewHolder, scanResult: ScanResult, position: Int) {
        holder.linearInfoPanel.setOnClickListener {
            if(listener!=null){
                listener!!.onClickWifi(position)
            }
        }
        holder.tvName.text = scanResult.SSID
        holder.tvNumber.text = "0"
    }

    private inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val linearInfoPanel: RelativeLayout = itemView.findViewById(R.id.linearWIFI)
        val tvName: TextView = itemView.findViewById(R.id.tvName)
        val tvNumber: TextView = itemView.findViewById(R.id.tvNumber)
    }

    fun setOnClickWIFI(listener: OnWifiClickListener) {
        this.listener = listener
    }

    interface OnWifiClickListener {
        fun onClickWifi(index: Int)
    }
}
