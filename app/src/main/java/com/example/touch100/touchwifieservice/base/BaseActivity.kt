package com.example.touch100.touchwifieservice.base

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.touch100.touchwifieservice.util.Contextor

class BaseActivity : AppCompatActivity(), View.OnClickListener{

    override fun onClick(p0: View?) {

    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

    }

    fun AppCompatActivity.replaceFragment(layoutId: Int, fragment: Fragment){
        supportFragmentManager.beginTransaction().replace(layoutId, fragment).commit()
    }
    fun AppCompatActivity.replaceFragmentwithBackStack(layoutId: Int, fragment: Fragment, tag: String?) {
        supportFragmentManager.beginTransaction().replace(layoutId, fragment, tag)
    }


}