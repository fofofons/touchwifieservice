package com.example.touch100.touchwifieservice.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.widget.Toast
import com.android.potioneer.custom.FragmentStateManager
import com.android.potioneer.ui.main.HolderFragment
import com.example.touch100.touchwifieservice.R
import com.example.touch100.touchwifieservice.util.Contextor
import com.example.touch100.touchwifieservice.view.CustomBottomTab
import kotlinx.android.synthetic.main.activity_main_menu.*

class MainMenuActivity : AppCompatActivity() {

    private lateinit var fragmentStateManager: FragmentStateManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        initFragmentState()
        initInstance()

    }

    private fun initFragmentState() {
        fragmentStateManager = object : FragmentStateManager(contentContainer, supportFragmentManager) {
            override fun getItem(position: Int): Fragment {
                return when (position) {
                    0 -> HolderFragment.newInstance(R.id.ivFirst)
                    1 -> HolderFragment.newInstance(R.id.ivSecond)
                    2 -> HolderFragment.newInstance(R.id.ivThird)
                    3 -> HolderFragment.newInstance(R.id.ivFourth)
                    4 -> HolderFragment.newInstance(R.id.ivFift)
                    else -> {
                        HolderFragment.newInstance(R.id.ivFirst)
                    }
                }
            }

        }
    }

    private fun initInstance() {
        fragmentStateManager.changeFragment(0)

        bottomTab.setOnTabClickListener(object : CustomBottomTab.TabListener {
            override fun onTabClick(position: Int) {
//                Log.d("Fonn", "Position : $position")
//                fragmentStateManager.removeFragment(position)
                fragmentStateManager.changeFragment(position)
            }
        })

    }
}
