package com.android.potioneer.custom

import android.support.v4.app.Fragment

interface FragmentNavigation {

    fun open(fragment: Fragment)

    fun replace(fragment: Fragment, addToBackStack: Boolean)

    fun navigateBack()
}
