package com.example.touch100.touchwifieservice.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import com.example.touch100.touchwifieservice.R

class CustomBottomTab: FrameLayout, View.OnClickListener{

    lateinit var ivFirst: ImageView
    lateinit var ivSecond: ImageView
    lateinit var ivThird: ImageView
    lateinit var ivFourth: ImageView
    lateinit var ivFift: ImageView

    private var tabListener: TabListener? = null
    private var position: Int = 0

    constructor(context: Context) : super(context) {
        initInflate()
        initInstance()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initInflate()
        initInstance()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initInflate()
        initInstance()
    }

    @SuppressLint("NewApi")
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        initInflate()
        initInstance()
    }

    private fun initInflate() {
        View.inflate(context, R.layout.view_bottom_tab, this)
    }

    private fun initInstance() {
        ivFirst = findViewById(R.id.ivFirst)
        ivSecond = findViewById(R.id.ivSecond)
        ivThird = findViewById(R.id.ivThird)
        ivFourth = findViewById(R.id.ivFourth)
        ivFift = findViewById(R.id.ivFift)

        ivFirst.setOnClickListener(this)
        ivSecond.setOnClickListener(this)
        ivThird.setOnClickListener(this)
        ivFourth.setOnClickListener(this)
        ivFift.setOnClickListener(this)

    }

    fun setOnTabClickListener(listener: TabListener){
        this.tabListener = listener
    }

    override fun onClick(view: View?) {
        when(view!!.id){
            R.id.ivFirst -> {
                setMenuSelected(ivFirst)
                position = 0
                if(tabListener != null){
                    tabListener!!.onTabClick(position)
                }
            }
            R.id.ivSecond -> {
                setMenuSelected(ivSecond)
                position = 1
                tabListener!!.onTabClick(position)

            }
            R.id.ivThird -> {
                setMenuSelected(ivThird)
                position = 2
                tabListener!!.onTabClick(position)
            }
            R.id.ivFourth -> {
                setMenuSelected(ivFourth)
                position = 3
                tabListener!!.onTabClick(position)
            }
            R.id.ivFift -> {
                setMenuSelected(ivFift)
                position = 4
                tabListener!!.onTabClick(position)
            }
        }
    }

    private fun setMenuSelected(imageView: ImageView){
        setColorMenu(imageView)
        setEnableMenu(imageView)
    }

    private fun setColorMenu(imageView: ImageView){
        ivFirst.setImageResource(R.drawable.ic_wifi_inactive)
        ivSecond.setImageResource(R.drawable.ic_chart_inactive)
        ivThird.setImageResource(R.drawable.ic_ping_inactive)
        ivFourth.setImageResource(R.drawable.ic_ftp_inactive)
        ivFift.setImageResource(R.drawable.ic_speed_inactive)

        when (imageView.id) {
            R.id.ivFirst -> {
                imageView.setImageResource(R.drawable.ic_wifi_active)
            }
            R.id.ivSecond -> {
                imageView.setImageResource(R.drawable.ic_chart_active)
            }
            R.id.ivThird -> {
                imageView.setImageResource(R.drawable.ic_ping_active)
            }
            R.id.ivFourth -> {
                imageView.setImageResource(R.drawable.ic_ftp_active)
            }
            R.id.ivFift -> {
                imageView.setImageResource(R.drawable.ic_speed_active)
            }
        }
    }

    private fun setEnableMenu(imageView: ImageView){
        ivFirst.isEnabled = true
        ivSecond.isEnabled = true
        ivThird.isEnabled = true
        ivFourth.isEnabled = true
        ivFift.isEnabled = true

//        imageView.isEnabled = false
    }

    interface TabListener{
        fun onTabClick(position: Int)
    }

}